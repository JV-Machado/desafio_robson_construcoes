﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Robson_Construcoes.Entities
{
    class Cargo
    {
        public Cargo[] Vetor_Cargo { get; set; }
        public double SalarioCargo { get; set; }
        public int Indice { get; set; }

        public Cargo()
        {
            Vetor_Cargo = new Cargo[5];
        }

        public Cargo(double salario_cargo)
        {
            SalarioCargo = salario_cargo;
        }

        public void CadastroCargo(Cargo carg)
        {
            Vetor_Cargo[Indice] = carg;
            Indice++;
        }

        public void Selecionar(Funcionario f, Cargo a)
        {
            Console.WriteLine("Selecione um cargo (0/1/2/3/4) para verificar o salário total pago à esse grupo:");
            string c = Console.ReadLine();
            if (c == "0")
            {
                SomarValorCargo(f, a.Vetor_Cargo[0]);
            }
            else if (c == "1")
            {
                SomarValorCargo(f, a.Vetor_Cargo[1]);
            }
            else if (c == "2")
            {
                SomarValorCargo(f, a.Vetor_Cargo[2]);
            }
            else if (c == "3")
            {
                SomarValorCargo(f, a.Vetor_Cargo[3]);
            }
            else if (c == "4")
            {
                SomarValorCargo(f, a.Vetor_Cargo[4]);
            }
            else
            {
                Console.WriteLine("Cargo Inexistente");
            }
        }
        public void SomarValorCargo(Funcionario f, Cargo a)
        {

            double salario = 0;
            for(int i = 0; i < f.Cont; i++)
            {
                if (f.Vetor_Funcionario[i].CargoF.SalarioCargo == a.SalarioCargo)
                {
                    salario += a.SalarioCargo;
                }
            }
            Console.WriteLine($"Total do cargo: {salario}");
            
        }
    }
}
