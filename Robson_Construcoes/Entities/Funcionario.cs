﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Robson_Construcoes.Entities
{
    class Funcionario : Cargo
    {
        public Funcionario[] Vetor_Funcionario { get; set; }
        public Cargo CargoF { get; set; }
        public string Nome { get; set; }
        public int CodigoFuncionario { get; set; }
        public int IndiceF { get; set; }
        public int Cont { get; set; }

        public Funcionario()
        {
            Vetor_Funcionario = new Funcionario[10];
            IndiceF = 0;
            Cont = 0;
        }

        public Funcionario(string nome, int cod_func, Cargo f)
        {
            Nome = nome;
            CodigoFuncionario = cod_func;
            CargoF = f;
        }

        public void CadastroFuncionario(Funcionario F)
        {

            for (int j = 0; j <= IndiceF; j++)
            {
                if (Vetor_Funcionario[j] == null)
                {
                    Vetor_Funcionario[IndiceF] = F;
                    Cont++;
                }
                else if(Vetor_Funcionario[j].CodigoFuncionario == F.CodigoFuncionario)
                {
                    return;
                }
                else if(Vetor_Funcionario[j].CodigoFuncionario != F.CodigoFuncionario && IndiceF == j)
                {
                    IndiceF++;
                    Vetor_Funcionario[IndiceF] = F;
                    Cont++;
                }
            }
        }

        public void Listar()
        {
            Console.WriteLine("Relatório:");
            for (int i = 0; i < Cont; i++)
            {
                Console.WriteLine(Vetor_Funcionario[i]);
            }
            Console.WriteLine();
        }

        public override string ToString()
        {
            return ($"Nome: {Nome} || Código do Funcionário: {CodigoFuncionario} || Salario: {CargoF.SalarioCargo}");
        }
    }
}

