﻿using System;
using Robson_Construcoes.Entities;

namespace Robson_Construcoes
{
    class Program
    {
        static void Main(string[] args)
        {
            Cargo cargo_func = new Cargo();

            Cargo cargo0 = new Cargo(2500.00);
            Cargo cargo1 = new Cargo(1500.00);
            Cargo cargo2 = new Cargo(10000.00);
            Cargo cargo3 = new Cargo(1200.00);
            Cargo cargo4 = new Cargo(800.00);

            cargo_func.CadastroCargo(cargo0);
            cargo_func.CadastroCargo(cargo1);
            cargo_func.CadastroCargo(cargo2);
            cargo_func.CadastroCargo(cargo3);
            cargo_func.CadastroCargo(cargo4);

            Funcionario cadastro_func = new Funcionario();

            Funcionario func1 = new Funcionario("João da Silva", 15, cargo1);
            Funcionario func2 = new Funcionario("Pedro Santos", 1, cargo2);
            Funcionario func3 = new Funcionario("Maria Oliveira", 26, cargo3);
            Funcionario func4 = new Funcionario("Rita Alcântara", 12, cargo1);
            Funcionario func5 = new Funcionario("Lígia Matos", 26, cargo2);

            cadastro_func.CadastroFuncionario(func1);
            cadastro_func.CadastroFuncionario(func2);
            cadastro_func.CadastroFuncionario(func3);
            cadastro_func.CadastroFuncionario(func4);
            cadastro_func.CadastroFuncionario(func5);

            cadastro_func.Listar();

            cargo_func.Selecionar(cadastro_func, cargo_func);
        }
    }
}
